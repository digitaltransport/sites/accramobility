# AccraMobility website

Published on [sites.digitaltransport.io/accramobility](http://sites.digitaltransport.io/accramobility/).  
Built with the [Agency theme by Start Bootstrap](https://github.com/BlackrockDigital/startbootstrap-agency).  
Powered by [Gitlab Pages](https://about.gitlab.com/product/pages/).  
Hosted by [Digital Transport for Africa](http://digitaltransport4africa.org/).

**Find all resources (GTFS dataset, maps..) from AccraMobility [on this repository](https://git.digitaltransport4africa.org/places/accra/).**

## Context

This original website was created in 2017 by [Etienne David](https://twitter.com/etiennedavid) at the [French Developement Agency](https://afd.fr/) to present the AccraMobility initiative. It was then recreated, updated and adapted to Gitlab by [Johan Richer](https://git.digitaltransport4africa.org/johan) in 2018.

This website is destined to be used as a template for other Digital Transport for Africa initiatives. Fork it and improve it!

## How to use this website as a template to build my own?

- [Fork this repository](https://git.digitaltransport4africa.org/sites/accramobility/forks/new).
- Follow [usage instructions](https://github.com/BlackrockDigital/startbootstrap-agency#usage) from the original authors and modify this template to suits your taste and needs.
- Use the [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/) feature to publish it.
- [Ask for help](mailto:johan.richer@jailbreak.paris) if needed!
